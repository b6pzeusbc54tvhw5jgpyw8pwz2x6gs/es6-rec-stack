# rec-stack
webclient & api server boilerplate project using [React][react],
[Expressjs][expressjs], [css-modules][css-modules].  This project was
bootstrapped with [Create React App][create-react-app]

## 1. 사전준비
- nodejs 8.9.4
- yarn >=1.3.2
- docker (optional, db 연동 예제 실행을 위해 필요)

## 2. 서드파티 패키지 설치
```
$ yarn
```

## 3. 환경변수 셋팅
개발환경에서 사용할 환경변수 셋팅이 필요하다. `.envrc.example` 파일을 참고하여
환경변수 셋팅 파일을 작성하여 현재 쉘에 셋팅하자.

### macOS or Linux 사용자
`export` 를 사용하고 파일이름을 `.envrc` 로 저장. 예:
```shell
export NODE_ENV=development
```

`source` 를 사용하여 현재 쉘에 환경변수 셋팅:
```shell
$ source .envrc
```

셋팅된 환경변수 확인:
```shell
$ env
```

### Windows 사용자
windows 에서는 `set` 을 사용하고 파일이름을 `.envrc.bat` 로 저장. 예:
```
set NODE_ENV=development
```

`.envrc.bat` 파일을 실행하여 현재 쉘에 환경변수를 셋팅:
```
> .envrc.bat
```

셋팅된 환경변수 확인:
```
> set
```

## 4. 개발서버 실행
```shell
# 서버 개발환경 실행
$ yarn server:start

# 클라이언트 개발환경 실행
$ yarn client:start

# 서버, 클라이언트 동시에 실행
$ yarn start
```

## 5. 파일 구조 설명
```
README.md           : 지금 보고있는 가이드 문서
.babelrc            : server 와 client test 에서 참조하는 babel 설정.
.envrc              : 환경변수 셋팅을 위한 파일. macOS, Linux 용.
.envrc.bat          : 환경변수 셋팅을 위한 파일. Windows 용.
.envrc.example      : 환경변수 예제. 디폴트라고 간주해도 좋음.
.gitignore          : git 용 ignored 파일 리스트.

docker-compose.yml  : 테스트 db 를 docker container 로 실행하기위한 정의 파일.
testdump.sql        : 테스트 db 에 사용할 더미 데이터.

package.json        : 애플리케이션의 기본정보, 스크립트, 종속라이브러리등의 정의 파일.
yarn.lock           : javascript 서드파티모듈 버전관리를 위한 자동생성파일.

▸ build/            : babel 로 transform 한 서버코드가 저장되며
                      실제 서버실행시 여기의 파일들이 사용됨.
▸ bundled/          : webpack output 으로 나온 client 코드산출물이 저장됨.
▸ config/           : webpack, webpackDevServer, nodemon, polyfill 등의 설정들.
▸ node_modules/     : yarn 을 통해 인터넷으로부터 내려받은 서드파티 javascript 패키지들.
▸ public/           : image 등의 static 파일.
▸ scripts/          : build, start, test 에 사용되는 script 파일들.
▾ src/              : 애플리케이션의 모든 소스 코드.
  ▾ bin/
      www.js        : 서버 실행시 entry point. express 객체를 import 하여 실행시킴.
  ▸ component/      : client 의 View layer, react component. 
  ▸ controller/     : server 의 비지니스 로직. route 가 사용하고 model 을 사용함
  ▸ helper/         : 구조 외의 util 역할등을 하는 모듈들.
                      server, client 양쪽에서 사용될 수 있음.
  ▸ middleware/     : express middleware.
  ▸ model/          : db 스키마를 model 로 추상화하여 server 쪽에서 사용.
                      Controller 에 의해 사용되어져야함.
  ▸ route/          : server 가 받는 http 요청들
    client.js       : client 의 엔트리 포인트.
    server.js       : express 프레임워크의 엔트리 포인트.
                      용도에 따라 다른 엔트리 파일들로 import 되어 실행됨.
```
server, client 코드를 별개의 디렉토리로 구분하지 않은 이유는 동일 담당자가
server, client 개발을 동시 진행할때 더 효율적인 구조를 고려함.  또한 미래에
universal javascript, server side rendering 의 도입으로 component 나 client 의
비지니스 로직들이 server side 에서 실행 될 수 있으므로 server 와 client 코드를
나누지 않고 모두 `src` 디렉토리 밑에 두었음.

server, client 담당자나 개발팀이 다르고 앞으로도 조직이 통합될 기미가 보이지
않는다면 이 프로젝트 보다 server, client 각각의 boilerplate 프로젝트를 찾아
진행하는 것을 강력하게 추천.

## 6. 서버 테스트
서버는 http api 를 통해 클라이언트의 동작여부와 상관없이 독립적으로 테스트 가능
```
$ curl 127.0.0.1:3030/ping
```

Windows 예제:
```
> Invoke-WebRequest http://127.0.0.1:3030/ping
```

## 7. DB 테스트
server 와 db 연동테스트 예제를 실행하기 위해 docker 를 통해 mysql 를 띄우고
테스트 데이터를 입력 한 뒤 table 출력하여 db 연동을 확인해보자: 
```
$ docker-compose up -d db
$ docker-compose run --rm db-init
$ docker-compose run --rm db-test
$ docker-compose run --rm db-rhel
```

## 8. TODO
- [ ] logger (maybe trace, loglevel)
- [ ] http request error handle example
- [ ] for production
    - [ ] production build
    - [ ] production executable script
    - [ ] CI & CD script (maybe .gitlab-ci.yml or Jenkinsfile)
- [ ] unit test
    - [ ] server unit test (maybe supertest)
    - [ ] client unit test (maybe jest or mocha)

[create-react-app]: https://github.com/facebookincubator/create-react-app
[react]: https://reactjs.org/
[expressjs]: https://expressjs.com/
[css-modules]: https://github.com/css-modules/css-modules
