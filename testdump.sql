CREATE TABLE Users (
  id varchar(255) NOT NULL PRIMARY KEY,
  name varchar(255),
  age int
  -- createdAt datetime,
  -- updateAt datetime,
);

INSERT INTO Users VALUES ('a', 'yoo', 20);
INSERT INTO Users VALUES ('b', 'kim', 21);
INSERT INTO Users VALUES ('c', 'lee', 22);
INSERT INTO Users VALUES ('d', 'park', 23);
