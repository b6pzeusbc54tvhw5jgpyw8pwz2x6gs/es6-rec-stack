import express from 'express';
import path from 'path';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import notFoundHandler from './middleware/notFoundHandler';
import errorHandler from './middleware/errorHandler';
import rootRouter from './route/index';
import userRouter from './route/user';

const app = express();

const { WEBPACK_DEV_SERVER_HOST, WEBPACK_DEV_SERVER_PORT } = process.env;

if( process.env.NODE_ENV === 'development' ) {
  const whitelist = [
    void 0,
    `http://${WEBPACK_DEV_SERVER_HOST}:${WEBPACK_DEV_SERVER_PORT}`,
    `https://${WEBPACK_DEV_SERVER_HOST}:${WEBPACK_DEV_SERVER_PORT}`,
  ];
  const corsOptions = {
    origin: function (origin, callback) {
      console.log(origin);
      if (whitelist.indexOf(origin) !== -1) callback(null, true);
      else callback(new Error('Not allowed by CORS'));
    }
  }
  app.use(cors(corsOptions));
}

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', rootRouter);
app.use('/api/user', userRouter);

// catch 404 and forward to errorHandler
app.use( notFoundHandler );
app.use( errorHandler );

export default app;
