import express from 'express';
import { getUserList, getUser } from '_src/controller/user';
const router = express.Router();

router.use('/getList', (req, res, next) => {
  getUserList().then( list => res.send( list ));
});

router.use('/get/:id', (req, res, next) => {
  const { id } = req.params;
  getUser( id ).then( user => res.send( user ));
});

export default router;
