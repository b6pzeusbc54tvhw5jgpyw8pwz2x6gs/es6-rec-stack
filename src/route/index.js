import express from 'express';
import { getPong, getVersion } from '_src/controller';
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.sendFile( process.env.INDEX_HTML_PATH );
});

router.use('/version', (req, res, next) => {
  // asynchronous job
  getVersion().then( version => res.send(version) );
});

router.use('/ping', (req, res, next) => {
  // synchronous job
  const pong = getPong();
  res.send( pong );
});

export default router;
