import { findAll, findOneById } from '_src/model/user';

export const getUserList = () => {
  return findAll();
};

export const getUser = (id) => {
  return findOneById(id);
};

