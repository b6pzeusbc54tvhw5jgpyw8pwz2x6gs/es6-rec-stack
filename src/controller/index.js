export const getVersion = () => new Promise( (resolve,reject) => {

  setTimeout( () => {
    const version = {
      serverVersion: process.env.SERVER_VERSION,
    };
    resolve( version );
  }, 2000 );
})

export const getPong = () => 'pong';
