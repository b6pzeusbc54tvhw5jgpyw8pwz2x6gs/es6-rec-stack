import Sequelize from 'sequelize';

const { DB_HOST, DB_NAME, DB_USER, DB_PASSWORD } = process.env;

export default new Sequelize( DB_NAME, DB_USER, DB_PASSWORD, {
  host: DB_HOST,
  port: 3306,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});
