import Sequelize from 'sequelize';
import sequelize from './index';

const User = sequelize.define('Users', {
  id: {
    type: Sequelize.STRING,
    primaryKey: true
  },
  name: Sequelize.STRING,
  age: Sequelize.INTEGER,
}, {
  timestamps: false,
});


export const findAll = () => {
  return User.findAll();
};

export const findOneById = (id) => {
  return User.findOne({ where: { id }});
};
