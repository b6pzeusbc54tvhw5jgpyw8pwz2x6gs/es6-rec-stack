import React, { Component } from 'react';
import st from './StateSample.css';
import api from '_src/helper/api';

export default class StateSample extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      pingPending: false,
      ping: '',
      versionPending: false,
      version: {},
      userListPending: false,
      userList: [],

      userId: '',
      userPending: false,
      user: {},
    };
  }

  getPong = () => {
    this.setState({ pingPending: true, ping: '' });
    api.get('/ping').then( res => {
      const pong = res.data;
      this.setState({ pingPending: false, ping: pong });
    });
  }

  getVersion = () => {
    this.setState({ versionPending: true, version: {} })
    api.get('/version').then( res => {
      const version = res.data;
      this.setState({ versionPending: false, version });
    });
  }

  getUserList = () => {
    this.setState({ userListPending: true, userList: [] })
    api.get('/api/user/getList').then( res => {
      const userList = res.data;
      this.setState({ userListPending: false, userList });
    });
  }

  getUser = (id) => {
    this.setState({ userPending: true, user: {} })
    api.get(`/api/user/get/${id}`).then( res => {
      const user = res.data;
      this.setState({ userPending: false, user });
    });
  }

  onChangeInput = (e) => {
    this.setState({ userId: e.target.value });
  }

  componentWillMount() {
    this.getPong();
    this.getVersion();
    this.getUserList();
  }

  render() {
    return (
      <div className={st.box}>
        <div>
          <pre style={{ margin: 0 }}>{JSON.stringify( this.state,null,2)}</pre>
        </div>
        <button onClick={this.getPong}>getPong</button>
        <button onClick={this.getVersion}>getVersion</button>
        <button onClick={this.getUserList}>getUserList</button>

        <input onChange={this.onChangeInput} value={this.state.userId}/>
        <button onClick={() => this.getUser(this.state.userId)}>getUser</button>
      </div>
    );
  }
};
