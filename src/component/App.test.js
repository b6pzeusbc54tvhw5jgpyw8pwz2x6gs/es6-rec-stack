import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import api from '_src/helper/api';

var MockAdapter = require('axios-mock-adapter');
var mock = new MockAdapter(api);

mock.onGet('/version').reply(200, { serverVersion: '99.99.99-test' });
mock.onGet('/ping').reply(200, 'pong' );
mock.onGet('/api/user/getList').reply(200, [{ id: 'a', name: 'yoo', age: 20 }]);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
