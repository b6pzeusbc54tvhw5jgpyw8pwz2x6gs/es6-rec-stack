import React, { Component } from 'react';
import { hot, setConfig } from 'react-hot-loader';
import logo from '_public/logo.svg';
import samplegif from '_public/sample.gif';
import samplebase64Url from '_public/small-image.png';
import st from './App.css';
import StateSample from './StateSample';
setConfig({ logLevel: 'log' })

class App extends Component {
  render() {
    return (
      <div className={st.App}>
        <header className={st.header}>
          <img src={logo} className={st.logo} alt="logo" />
          <h1 className={st.title}>Welcome to React</h1>
        </header>
        <p className={st.intro}>src/component/App.js</p>

        <h3>1. StateSample</h3>
        <StateSample />

        <h3>2. url-loader big image file test</h3>
        <img src={samplegif} alt="gif sample" />

        <h3>3. url-loader small image(base64url) file test</h3>
        <img src={samplebase64Url} alt="gif sample" />

        <h3>4. url-loader in css-modules big image file test</h3>
        <div className={st.jpgBg}>background image sample</div>

        <h3>5. url-loader in css-modules small image(base64url) file test</h3>
        <div className={st.base64UrlBg}>background image sample</div>
      </div>
    );
  }
}

export default hot(module)(App);
